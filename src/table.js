const connection = require('./connection')();
module.exports = (table) =>{
    return {
        sql:{
            select:'*',
            wheres: '',
            limit: null,
            innerJoin: null
        },
        where(options=[]){
            if(this.sql.wheres === '' && options.length > 0){
                this.sql.wheres = 'WHERE ';
            }
            options.forEach((option, index)=>{
                if(index==0){
                    this.sql.wheres+=`${option[0]} ${option[1]} ${option[2]}`;
                }else{
                    this.sql.wheres+=` AND ${option[0]} ${option[1]} ${option[2]}`;
                }
            });
            return this;
        },
        select(options){
            this.sql.select = options;
            return this;
        },
        limit(limit){
            this.sql.limit = limit;
            return this;
        },
        get(){
            return new Promise((resolve, reject)=>{
                connection.get().then((con)=>{
                    let query = 'SELECT '+this.sql.select+' FROM '+table;
                    if(this.sql.wheres != ''){
                        query+=' '+this.sql.wheres;
                    }

                    if(this.sql.limit){
                        query+=' LIMIT '+this.sql.limit;
                    }
                    con.query(query, (error, res)=>{
                        if(error){
                            reject(error);
                        }
                        con.end();
                        resolve(res);
                    });
                }).catch((error)=>{
                    reject(error);
                })
            })
        },
        first(){
            return new Promise((resolve, reject)=>{
                connection.get().then((con)=>{
                    let query = 'SELECT '+this.sql.select+' FROM '+table;
                    if(this.sql.wheres != ''){
                        query+=' '+this.sql.wheres;
                    }

                    if(this.sql.limit){
                        query+=' LIMIT 1';
                    }
                    con.query(query, (error, res)=>{
                        if(error){
                            reject(error);
                        }
                        con.end();
                        resolve((res.length > 0)?res[0]:null);
                    });
                }).catch((error)=>{
                    reject(error);
                })
            })
        }
    }
}