const mysql = require('mysql');
const connections = require('./connection');
const table = require('./table');

module.exports = {
    connections,
    table
}