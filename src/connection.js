const config = require('./config');
const mysql = require('mysql');



module.exports = (driver=null)=>{
    let options;
    if(!driver){
        options = config.connections[config.default];
    }else{
        options = config.connections[driver];
    }
    const connection = mysql.createConnection(options);
    return {
        get(){
            return new Promise((resolve, reject)=>{
                connection.connect((error)=>{
                    if(error) reject(error);
                    resolve(connection);
                });
            })
        }   
    }
};